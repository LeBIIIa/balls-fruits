﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
namespace lines
{
    public partial class Form1 : Form
    {
        //Початок створення структур
        struct ball
        {
            public int state;
            public int color;
            public int num_pic;
        };
        struct ball_last
        {
            public int x;
            public int y;
            public int color;
            public bool check;
        };
        struct ballrandom
        {
            public int x;
            public int y;
            public int color;
        }
        struct ballTip
        {
            public int x;
            public int y;
        }
    //Кінець 
///////////////
        //Ініціалізація компонентів
        public Form1()
        {
            InitializeComponent();
        }
        //
///////////////
        //Ініціалізація змінних
        int MAX_X;
        int MAX_Y;
        int MAX_C;
        int f,f1;
        PictureBox[,] pics;
        ball[,] balls;
        ball_last global;
        ballrandom[] ballTipSaved;
        Bitmap[,] TableImage;
        ballTip[] MoveWay;
        #region Накопичувальні змінні
        int tick = 1;
        int CountTickTimer = 1; //вибрану позицію не анімувати, вона і так в активному стані
        int countBalls = 0;

        //Timer's variables
        int h;
        int m;
        int s;
        #endregion
        //Кінець Ініціалізації

////////////////
        //Генерація ігрового поля
        private void GenerateTable(int columnCount, int rowCount)
        {
            tableLayoutPanel1.Controls.Clear();
            tableLayoutPanel1.ColumnStyles.Clear();
            tableLayoutPanel1.RowStyles.Clear();
            tableLayoutPanel1.ColumnCount = columnCount;
            tableLayoutPanel1.RowCount = rowCount;
            for (int x = 0; x < columnCount; x++)
            {
                tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.AutoSize));
                for (int y = 0; y < rowCount; y++)
                {
                    if (x == 0)
                    {
                        tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.AutoSize));
                    }
                    PictureBox pic = new PictureBox();
                    pic.Width = 48;
                    pic.Height = 48;

                    pic.Margin = new Padding(0);
                    pic.Image = null;
                    pic.Tag = x.ToString() + " " + y.ToString();
                    pic.Click += ball_Click;
                    pic.BorderStyle = BorderStyle.FixedSingle;
                    pic.SizeMode = PictureBoxSizeMode.CenterImage; //для малих картинок-підказок
                    pics[x, y] = pic;
                    balls[x, y].state = 0;
                    balls[x, y].num_pic = 3;
                    tableLayoutPanel1.Controls.Add(pic, y, x);
                }
            }
        }//Кінець генерації

////////////////
        //Подія, Коли ми нажимаємо на кульку чи на фрукт
        void ball_Click(object sender, EventArgs e)
        {
            timer2.Enabled = true;
            if ( f == 0 )
            {
                PictureBox pic = sender as PictureBox;
                string s = pic.Tag as string;
                var k = s.Split(' ');

                int x = Convert.ToInt32(k[0]);
                int y = Convert.ToInt32(k[1]);
                if (balls[x, y].num_pic == 2 || balls[x, y].num_pic == 0)
                {
                    //Запис координат попереднього шарика + перемикання між шариками реалізовано тут
                    if (global.check)
                    {
                        balls[global.x, global.y].num_pic = 2;//1
                        balls[x, y].num_pic = 0; //2

                        global.x = x;
                        global.y = y;
                        global.color = balls[x, y].color;
                    }
                    else//при новій грі
                    {
                        global.x = x;
                        global.y = y;
                        global.check = true;
                        global.color = balls[x, y].color;
                        balls[x, y].num_pic = 0;
                    }
                }
                else //Нажато на вільне місце. Якщо є виділений шарик то здійснити рух. 
                {//За допомогою voln() - отримаємо шлях для переміщення     
                    if (global.check)
                    {
                        tableLayoutPanel1.Enabled = false;
                        f = 1;
                        MoveWay = voln(balls, x, y);//ВКЛ АНІМАЦІЮ ПЕРЕМІЩЕННЯ

                        if (MoveWay != null && MoveWay.Count() < 2)
                        {
                            f = 0;
                            MessageBox.Show("Помилка в шляху");
                        }

                        if (MoveWay != null)//якщо знайдено шлях
                        {
                            
                            balls[global.x, global.y].state = 0;//1
                            timer1.Enabled = true;
                            
                        }
                        else
                        {
                            f = 0;
                            MessageBox.Show("Шляху не знайдено!");
                            balls[global.x, global.y].num_pic = 2;
                        }
                        tableLayoutPanel1.Enabled = true;
                        global.check = false;
                    }
                    else
                    {
                        global.check = false;
                    }
                }
                Refresh1();
                
            }
            
        }//Кінець Події

////////////////
        //Генерація випадкових координат
        ballrandom RandomPosGenerate()
        {
            int x, y, color;
            Random rnd = new Random();
            do
            {
                x = rnd.Next(0, MAX_X);
                y = rnd.Next(0, MAX_Y);
                color = rnd.Next(0, MAX_C);
            } while (balls[x,y].state != 0 && balls[x,y].num_pic != 3);
            ballrandom br= new ballrandom();
            br.x = x;
            br.y = y;
            br.color = color;
            return br;
        }//Кінець Генерації

////////////////
        //Функція генерації підказок
        void ShowTips()
        {
            if (!(MAX_X * MAX_Y - countBalls < 3) )
            {
                //Показує три підказки на на полі
                for (int i = 0; i < 3; i++)
                {
                        ballrandom br = RandomPosGenerate();

                        balls[br.x, br.y].state = 1;
                        balls[br.x, br.y].color = br.color;
                        balls[br.x, br.y].num_pic = 1;

                    //Запам'ятати розкаташування підказок і колір
                        ballTipSaved[i].x = br.x;
                        ballTipSaved[i].y = br.y;
                        ballTipSaved[i].color = br.color;       
                }
                Refresh1();
            }
            else
            {
                timer2.Enabled = false;
                tableLayoutPanel1.Enabled = false;
                TheEnd tmp = new TheEnd(Convert.ToInt32(label2.Text),tick);
                tmp.ShowDialog();
            }
        }//Кінець функції

////////////////
        //Функція для перегенерування підказки
        //Якщо на шляху була підказка збереження її кольору
        void ShowTipBalls()
        {
            for (int i = 0; i < 3; i++)
            {
                if (balls[ballTipSaved[i].x, ballTipSaved[i].y].num_pic == 2) //коли шариком стали на підказку, то це означає перегенерувати  і-ту підказку і перезаписати її в ballTipSaved
                {
                    ballrandom br = RandomPosGenerate();

                    balls[br.x, br.y].state = 1;
                    balls[br.x, br.y].num_pic = 1;

                    //Запам'ятати розкаташування підказок і колір
                    ballTipSaved[i].x = br.x;
                    ballTipSaved[i].y = br.y;

                    balls[ballTipSaved[i].x, ballTipSaved[i].y].num_pic = 2;
                    balls[ballTipSaved[i].x, ballTipSaved[i].y].color = ballTipSaved[i].color;// вернути колір (після затирання комірки)
                }
                else
                {
                    balls[ballTipSaved[i].x, ballTipSaved[i].y].num_pic = 2;
                    balls[ballTipSaved[i].x, ballTipSaved[i].y].color = ballTipSaved[i].color;// вернути колір (після затирання комірки)
                }
            findlines(ballTipSaved[i].x, ballTipSaved[i].y);
            }
            countBalls += 3;
            Refresh1();
        }//Кінець функції

////////////////
        //Створення нової гри
        void newGame()
        {
            if (timer2.Enabled)
            {
                tick = 1;
            }
            else
                timer2.Enabled = true;

            CountTickTimer = 1;
            label2.Text = "0";
            countBalls = 0;
            f = 0;
            f1 = 0;

            global.check = false;
            /*1.очистити поле balls pics
            3 рази RandomPosGenerate
             * ставим 3 шарики по відпов коорд.
             * обновляєм pics
             * скинути очки балів */
            ClearSpace();

            for (int i = 0; i < 3; i++)
            {
                ballrandom br = RandomPosGenerate();

                balls[br.x, br.y].state = 1;
                balls[br.x, br.y].color = br.color;
                balls[br.x, br.y].num_pic = 2;
                countBalls++;
            }
            Refresh1();


        }//Кінець створення

////////////////
        //Перезавантаження таблиці
        void Refresh1()
        {
            for (int i = 0; i < MAX_X; i++)
            {
                for (int j = 0; j < MAX_Y; j++)
                {
                    if (balls[i, j].num_pic != 3) //balls[i, j].state != 0 && 
                        pics[i, j].Image = GetImage(balls[i, j].num_pic, balls[i, j].color);
                    else
                        pics[i, j].Image = null;
                }
            }
        }//Кінець перезавантаження

////////////////
        //Очищення таблиці
        void ClearSpace()
        {
            for (int i = 0; i < MAX_X; i++)
            {
                for (int j = 0; j < MAX_Y; j++)
                {
                    balls[i, j].num_pic = 3;
                    balls[i, j].state = 0;
                }
            }
            Refresh1();
        }//Кінець очищення

////////////////
        //Отримання картинки за станом та кольором
        Bitmap GetImage(int num_pic, int color)
        {
            return TableImage[num_pic, color];
        }//Кінець функції

////////////////
        //Створення масивів для кульок/фруктів та підказок
        private void Form1_Load(object sender, EventArgs e)
        {
            MAX_X = Properties.Settings.Default.MAX_X;
            MAX_Y = Properties.Settings.Default.MAX_Y;
            MAX_C = Properties.Settings.Default.MAX_C;

            balls = new ball[MAX_X, MAX_Y];

            ballTipSaved = new ballrandom[3];

            pics = new PictureBox[MAX_X, MAX_Y];

            global = new ball_last();
            
            TableImage = new Bitmap[3, MAX_C];

            ShowFruits();
            фруктиToolStripMenuItem.Checked = true;
            x13ToolStripMenuItem.Checked = true;
            tableLayoutPanel1.Enabled = true;
            this.MaximizeBox = false;

            GenerateTable(MAX_X, MAX_Y);

            label2.Text = "0";
            label1.Text = "00:00:00";
            CountTickTimer = 1;
            countBalls = 0;
            
        }//Кінець створення масивів

////////////////
        //Завантаження нових масивів чи 9x9/13x13/15x15
        public void loadstruct(int MAX_X,int MAX_Y,int MAX_C)
        {
            balls = new ball[MAX_X, MAX_Y];

            pics = new PictureBox[MAX_X, MAX_Y];

            global = new ball_last();

            GenerateTable(MAX_X, MAX_Y);

            label2.Text = "0";
            CountTickTimer = 1;
            countBalls = 0;
        }//Кінець створення нових масивів

////////////////
        //Хвильовий алгоритм
        ballTip[] voln(ball[,] ball_temp,int x,int y)
        {
            //Перенести ще в map масив balls
            //Але коли переносити, то в мап поставити бордюри(рамку, а в середену загнати balls)
            int n, m, i, j, k,i1,j1;

            i1 = x + 1 ;// @"+1" - бо це координати для шариків, де поле не має бордюрів
            j1 = y + 1 ;

            int[,] map = new int[MAX_X+2, MAX_Y+2];
            n = MAX_X;
            m = MAX_Y;
            //Якщо поле для шариків буде прямокутним, то окремо рядки і стовці заповню межами
            for (i = 0; i < n+2; i++)
            {
                map[0, i] = 1;
                map[n + 2 - 1, i] = 1;
            }
            for (i = 0; i < m+2; i++)
            {
                map[i, 0] = 1;
                map[i, m + 2 - 1] = 1;
            }
            for (i = 1; i <= n; i++)
            {
                for (j = 1; j <= m; j++)
                {
                    if (balls[i - 1, j - 1].num_pic == 1)
                    {
                        map[i, j] = 0;
                    }
                    else
                        map[i, j] = balls[i-1, j-1].state;
                }
            }


            int f = 1;
            k = 2;
            map[global.x+1,global.y+1] = k;

            while (f!=0)
            {
                f = 0;
                for (i = 1; i <= n; i++)
                {
                    for (j = 1; j <= m; j++)
                    {
                        if (map[i, j] == 0 && (map[i - 1, j] == k || map[i + 1, j] == k || map[i, j - 1] == k || map[i, j + 1] == k))
                        {
                            map[i, j] = k + 1;
                            if (i == i1 && j == j1)
                            {
                                f = 0;
                                i = n + 1;
                                break;
                            }
                            else
                                f = 1;
                        }
                        
                    }
                }
                k++;

                if (f==0) //!f
                    break;
            }
            k--;


            ballTip[] rez = new ballTip[++k-1];

            if (map[i1, j1] != 0)
            {
                i = i1; j = j1;
                while (k >= 2)
                {

                    rez[k - 2].x = i-1;
                    rez[k - 2].y = j-1;
                    k--;
                    
                    if (map[i - 1, j] == k) i--;
                    if (map[i + 1, j] == k) i++;
                    if (map[i, j - 1] == k) j--;
                    if (map[i, j + 1] == k) j++;
                }
            }
            else
                return null;
            return rez;//p.-st'
        }//Кінець алгоритму

////////////////
        //Анімація пересування кульок
        private void timer1_Tick(object sender, EventArgs e)
        {
            if (CountTickTimer == MoveWay.Count())
            {
                
                timer1.Enabled = false;
                ShowTipBalls();
                f1 = 0;
                findlines(MoveWay[CountTickTimer - 1].x, MoveWay[CountTickTimer - 1].y);
                if ( f1==0 )
                { 
                    ShowTips();
                }
                
                Refresh1();
                

                //Чи не утворилась лінія
                

                CountTickTimer = 1;
                f = 0;
                return;
            }
            //Попередній шарик сховати
            int colorTip = checkTips(MoveWay[CountTickTimer - 1].x, MoveWay[CountTickTimer - 1].y);
            if (colorTip != -1)
            {
                balls[MoveWay[CountTickTimer - 1].x, MoveWay[CountTickTimer - 1].y].num_pic = 1;
                balls[MoveWay[CountTickTimer - 1].x, MoveWay[CountTickTimer - 1].y].color = colorTip;
            }
            else//якщо на шляху нема підказки
            {
                balls[MoveWay[CountTickTimer - 1].x, MoveWay[CountTickTimer - 1].y].state = 0;
                balls[MoveWay[CountTickTimer - 1].x, MoveWay[CountTickTimer - 1].y].num_pic = 3;
            }

            //Новий показати
            balls[MoveWay[CountTickTimer].x, MoveWay[CountTickTimer].y].num_pic = 2; //2
            balls[MoveWay[CountTickTimer].x, MoveWay[CountTickTimer].y].state = 1;
            balls[MoveWay[CountTickTimer].x, MoveWay[CountTickTimer].y].color = global.color; //balls[global.x, global.y]
            Refresh1();

            CountTickTimer++;
            
            
        }//Кінець анімації 

////////////////
        //Якщо на шляху була підказка то повертаємо її колір
        int checkTips(int x,int y)
        {
            for (int i = 0; i < 3; i++)
            {
                if (ballTipSaved[i].x == x && ballTipSaved[i].y == y)
                {
                    return ballTipSaved[i].color;
                }
            }
            return -1;
        }//Кінець функції

////////////////
        //Перевіряє чи не утворилася лінія з кульок
        void findlines(int x, int y)
        {
            int d1 = 0,d2=0,d3=0,d4=0,d5=0,d6=0,d7=0,d8=0;
            int i = 1;
            /* Перевірка 
             * balls[x, y - i].num_pic != 3 
             * для єдиного випадку - коли походили зеленим шариком і зліва біля нього порожня комірка, то порожня комірка має занулені всі значення
             * а нуль для поля color означає ЗЕЛЕНИЙ. І виходить шо зелений постійно знаходить собі "компаньйона"
             */

            /*
             Треба перевірку ще "чи не підказка". Бо коли поставити біля підказки, то її тоже підраховує
             * 
             * balls[x, y - i].num_pic != 1
             * 
             * Але виходить що коли поставили шарик біля підказки, то підказка стає повноцінним шариком. То що робити? Коли знищувати лінію? Перед підказкою (без неї) чи після появи підказки - показати її і знищити з нею
             * А ось коли ми покалали шарик, а підказка появилась в цій лінії цього ж кольору, то її не треба враховувати, а в нас так і рахує
             */
            //-------------d1-------------
            while ( y - i >= 0
                && balls[x, y - i].state == 1
                && balls[x, y - i].color == balls[x, y].color) //on left @00X1@
            {
                d1++;
                i++;
            }

            //-------------d2-------------
            i = 1; 
            while (y + i < MAX_Y
                && balls[x, y + i].state == 1
                && balls[x, y + i].color == balls[x, y].color) //on right
            {
                i++;
                d2++;
            }
            //-------------d3-------------
            i = 1;
            while (x - i >=0
                && balls[x - i, y].state == 1
                && balls[x - i, y].color == balls[x, y].color) 
            {
                i++;
                d3++;
            }
            //-------------d4-------------
            i = 1;
            while (x + i < MAX_X
                && balls[x + i, y].state == 1
                && balls[x + i, y].color == balls[x, y].color) 
            {
                i++;
                d4++;
            }
            //-------------d5-------------
            i = 1;
            while (x - i >= 0
                && y - i >= 0
                && balls[x - i, y - i].state == 1
                && balls[x - i, y - i].color == balls[x, y].color) 
            {
                i++;
                d5++;
            }
            //-------------d6-------------
            i = 1;
            while (x - i >= 0
                && y + i < MAX_Y
                && balls[x - i, y + i].state == 1
                && balls[x - i, y + i].color == balls[x, y].color) 
            {
                i++;
                d6++;
            }
            //-------------d7-------------
            i = 1;
            while (x + i < MAX_X
                && y + i < MAX_Y
                && balls[x + i, y + i].state == 1
                && balls[x + i, y + i].color == balls[x, y].color) 
            {
                i++;
                d7++;
            }
            //-------------d8-------------
            i = 1;
            while (x + i < MAX_X
                && y - i >= 0
                && balls[x + i, y - i].state == 1
                && balls[x + i, y - i].color == balls[x, y].color) 
            {
                i++;
                d8++;
            }
            int count = 0;
            if (d1 + d2 >= 4)
                count += delete(x, y - d1, x, y + d2,12);
            if (d3 + d4 >= 4)
                count += delete(x - d3, y, x + d4, y, 34);
            if (d5 + d7 >= 4)
                count += delete(x - d5, y - d5, x + d7, y + d7, 57);
            if (d6 + d8 >= 4)
                count += delete(x - d6, y + d6, x + d8, y - d8, 68);
            if (count >= 4)
            {
                label2.Text =  (++count + Convert.ToInt32(label2.Text)).ToString();

                countBalls -= count;
            }

        }//Кінець Перевірки

////////////////
        //Видалення лінії з кульок
        int delete(int x1, int y1, int x2, int y2, int typeDel)
        {
            f1 = 1;
            int count = 0;
            switch (typeDel)
            {
                case 12:
                    for (int i = y1; i <= y2; i++)
                    {
                        balls[x1, i].num_pic = 3;
                        balls[x1, i].state = 0;
                        count++;
                    }
                    break;
                case 34:
                    for (int i = x1; i <= x2; i++)
                    {
                        balls[i, y1].num_pic = 3;
                        balls[i, y1].state = 0;
                        count++;
                    }
                    break;
                case 57:
                    for (int i = x1, j=y1; i <= x2; i++,y1++)
                    {
                        balls[i, y1].num_pic = 3;
                        balls[i, y1].state = 0;
                        count++;
                    }
                    break;
                case 68:
                    for (int i = x1, j=y1; i <= x2; i++,y1--)
                    {
                        balls[i, y1].num_pic = 3;
                        balls[i, y1].state = 0;
                        count++;
                    }
                    break;
            }
            Refresh1();
            return count-1; //"мінус центральна", бо коли буде перехрастя, то це (5-1)+(5-1)+1 = 4+4+1
        }//Кінець видалення 

////////////////
        //Часики
        private void timer2_Tick(object sender, EventArgs e)
        {
            h = tick / 3600;
            m = tick / 60 - h * 60;
            s = tick - m * 60 - tick / 3600;
            label1.Text = timelow(h) + ":" + timelow(m) + ":" + timelow(s);
            tick++;
        }//Кінець часів

////////////////
        //Якщо години/хвилини/секунди менше 10 то ставимо перед ними "0"
        string timelow(int tick)
        {
            if (tick > 9)
                return tick.ToString();
            else
                return "0" + tick;
        }//Кінець перетворення годин 

////////////////
        //Подія, Створення нової гри
        private void новаГраToolStripMenuItem_Click(object sender, EventArgs e)
        {
            newGame();
            ShowTips();
        }//Кінець події

////////////////
        //Подія, виходу з гри
        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }//Кінець події 

////////////////
        //Подія, Форма про розробника
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            About testDialog = new About();
            testDialog.Show(this);
        }//Кінець події

////////////////
        //Подія, показу фруктів
        private void фруктиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem k = sender as ToolStripMenuItem;
            k.Checked = true;
            кулькиToolStripMenuItem.Checked = false;

            ShowFruits();

            Refresh1();
        }//Кінець події

////////////////
        //Подія, показу кульок
        private void кулькиToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            ToolStripMenuItem k = sender as ToolStripMenuItem;
            k.Checked = true;
            фруктиToolStripMenuItem.Checked = false;

            ShowBalls();

            Refresh1();
        }//Кінець події 

////////////////
        //Показ кульок
        void ShowBalls()
        {
            TableImage[0, 0] = Properties.Resources._12;
            TableImage[0, 1] = Properties.Resources._22;
            TableImage[0, 2] = Properties.Resources._32;
            TableImage[0, 3] = Properties.Resources._42;
            TableImage[0, 4] = Properties.Resources._52;
            TableImage[0, 5] = Properties.Resources._62;

            TableImage[1, 0] = Properties.Resources._10;
            TableImage[1, 1] = Properties.Resources._20;
            TableImage[1, 2] = Properties.Resources._30;
            TableImage[1, 3] = Properties.Resources._40;
            TableImage[1, 4] = Properties.Resources._50;
            TableImage[1, 5] = Properties.Resources._60;

            TableImage[2, 0] = Properties.Resources._11;
            TableImage[2, 1] = Properties.Resources._21;
            TableImage[2, 2] = Properties.Resources._31;
            TableImage[2, 3] = Properties.Resources._41;
            TableImage[2, 4] = Properties.Resources._51;
            TableImage[2, 5] = Properties.Resources._61;
        }//Кінець функції

////////////////
        //Показ фруктів
        void ShowFruits()
        {
            TableImage[0, 0] = Properties.Resources.f12;
            TableImage[0, 1] = Properties.Resources.f22;
            TableImage[0, 2] = Properties.Resources.f32;
            TableImage[0, 3] = Properties.Resources.f42;
            TableImage[0, 4] = Properties.Resources.f52;
            TableImage[0, 5] = Properties.Resources.f62;

            TableImage[1, 0] = Properties.Resources.f10;
            TableImage[1, 1] = Properties.Resources.f20;
            TableImage[1, 2] = Properties.Resources.f30;
            TableImage[1, 3] = Properties.Resources.f40;
            TableImage[1, 4] = Properties.Resources.f50;
            TableImage[1, 5] = Properties.Resources.f60;

            TableImage[2, 0] = Properties.Resources.f11;
            TableImage[2, 1] = Properties.Resources.f21;
            TableImage[2, 2] = Properties.Resources.f31;
            TableImage[2, 3] = Properties.Resources.f41;
            TableImage[2, 4] = Properties.Resources.f51;
            TableImage[2, 5] = Properties.Resources.f61;
        }//Кінець функції    
 
////////////////
        //Змінна поля 9*9
        private void x9ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reset();
            MAX_X = 9;
            MAX_Y = 9;
            this.Width = 448;
            this.Height = 495;
            panel1.Location = new Point(this.Width - panel1.Width - MAX_X - 3, 1);
            loadstruct(MAX_X,MAX_Y,MAX_C);
            x9ToolStripMenuItem.Checked = true;
            x13ToolStripMenuItem.Checked = false;
            x15ToolStripMenuItem.Checked = false;
            newGame();
            ShowTips();
        }//Кінець зміни      

////////////////
        //Змінна поля 13*13
        private void x13ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reset();
            MAX_X = 13;
            MAX_Y = 13;
            this.Width = 640;
            this.Height = 687;
            panel1.Location = new Point (this.Width-panel1.Width-MAX_X-3,1);
            loadstruct(MAX_X, MAX_Y, MAX_C); 
            x9ToolStripMenuItem.Checked = false;
            x13ToolStripMenuItem.Checked = true;
            x15ToolStripMenuItem.Checked = false;
            newGame();
            ShowTips();

        }//Кінець зміни
        
////////////////
        //Змінна поля 11*11
        private void x11ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reset();
            this.Height = 591;
            this.Width = 544;
            MAX_X = 11;
            MAX_Y = 11;
            panel1.Location = new Point(this.Width - panel1.Width - MAX_X - 3, 1);
            loadstruct(MAX_X, MAX_Y, MAX_C);
            x9ToolStripMenuItem.Checked = false;
            x13ToolStripMenuItem.Checked = false;
            x15ToolStripMenuItem.Checked = true;
            newGame();
            ShowTips();
        }//Кінець зміни
////////////////
        //Звільняє всі поля
        public void reset ()
        {
            timer2.Enabled = false;
            tick = 0;
            label1.Text = "00:00:00";
            label2.Text = "0";
            h = 0;
            m = 0;
            s = 0;
        }
        //Кінець Звільненю
///////////////
    }
}
