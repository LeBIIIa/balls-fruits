﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace lines
{
    public partial class TheEnd : Form
    {
        public int tick;
        public int count;
        public int h;
        public int m;
        public int s;
        public TheEnd(int count, int tick)
        {
            InitializeComponent();
            this.tick = tick-1;
            this.count = count;
        }

        private void TheEnd_Load(object sender, EventArgs e)
        {
            label1.Text = "";
            label2.Text = "";
            label3.Text = "";
            pictureBox1.Location = new Point(this.Width/2-pictureBox1.Width/2,12);
            label1.Text = "Ви набрали: " + count + " балів";
            h = tick / 3600;
            m = tick / 60 - h * 60;
            s = tick - m * 60 - tick / 3600;
            if ( h==0 && m==0 && s>0 )
            {
                label2.Text = "Ви справились за: " + s +" сек";
            }
            else if (h==0 && m>0 && s>0)
            {
                label2.Text = "Ви справились за: " + m + " хв" + s + " сек";
            }
            else if (h>0 && m>0 && s>0)
            {
                label2.Text = "Ви справились за: " + h + " " + "год" + m + " " + " хв" + s + " "  + "сек";
            }
            else
            {
                label2.Text = "Ви справились за: " + h + " год" + m + " хв" + s + " сек";
            }
            if ( count>=125 )
            {
                Win_Lose test_msg = new Win_Lose();
                test_msg.Show();
            }
            else
            {
                label3.Text = "На превеликий жаль ви програли цю гру";
                label4.Text = "Щоб виграти потрібно набрати не менше 125 балів";
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
